"""
Information about the package version.
"""
version_info = (0, 16, 0)
__version__ = ".".join(map(str, version_info))
