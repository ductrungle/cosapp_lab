
==============
Installation
==============


The easiest way to obtain *cosapp_lab* is to install the conda package from *conda-forge* channel:

.. code-block:: shell  

    conda install -c conda-forge cosapp_lab

*cosapp_lab* is also available on PyPI. However, since *pythonocc-core* is not, users can install *cosapp_lab* with *pip*, but the 3D viewer widget will not work.

.. code-block:: shell  

    pip install cosapp-lab
 
*JupyterLab* is not a direct dependency of *cosapp_lab*, but users need to have JupyterLab (>3.0) in order to create CoSApp dashboard in notebooks.  



