#################################
CosApp Lab documentation
#################################

.. only:: html

   :Release: |release|
   :Date: |today|

.. toctree::
   :hidden:
   :caption: License



.. include:: readme.rst

.. toctree::
   :maxdepth: 2

   installation

   using

   deploy


.. toctree::
   :maxdepth: 1
   
   history

   authors
   
   license



Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

