=====================
Document widget
=====================

Document widget is a CoSApp Lab widget allowing users to add document into dashboard by using Markdown text. Users can only use it in **SysExplorer**.

---------------------------
Start Document widget 
---------------------------

To open Document widget inside **SysExplorer**, just select *Document widget* in widget menu of any section. 

------------------------------------
Document widget main interface
------------------------------------

**Document widget** contains a render window to show the documentation and a toolbar at the bottom of interface.

.. image:: ../img/Document_widget_main.png
   :width: 100%   

Editor dialog
======================

This dialog is used to type Markdown text of widget. Users can save the text into dashboard configuration and reload it in other document widget.

.. image:: ../img/Document_widget_editor.png
   :width: 100% 

Document list dialog
======================

This dialog allows users to select saved document to be shown in widget.
