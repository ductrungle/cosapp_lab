
.. image:: https://mybinder.org/badge_logo.svg
    :target: https://mybinder.org/v2/gl/cosapp%2Fcosapp_lab/master?urlpath=lab/tree/examples/SysExplorer.ipynb

===========================================================================
CosApp Lab - Toolbox for managing and deploying CoSApp powered dashboards.
===========================================================================



--------------
Introduction
--------------

The primary goal of **CoSApp Lab** is to help users transform existing CoSApp 
modules into interactive dashboards, with almost no additional development or
configuration.


Examples
==========



Using CosApp Lab in JupyterLab
------------------------------------

CoSApp Lab provides a JupyterLab extension named **SysExplorer** creating interactive dashboards. This extension allows users to dynamically customize the layout and fill their dashboard with multiple predefined widgets, such as charts, controllers (sliders, *etc.*), 3D visualization panels...

.. image:: ./img/cosapp_lab.gif
    :alt: CosApp Lab in JupyterLab
    :width: 800

Using CosApp Lab standalone mode
--------------------------------------

Dashboards defined with **SysExplorer** in JupyterLab can also be exported into libraries and served by CoSApp Lab as web applications.  

.. image:: ./img/cosapp_lab_all.gif
    :alt: CosApp Lab in JupyterLab
    :width: 800


------------------
Table of contents
------------------

A more detailed **CoSApp Lab** documentation is available at the sections below:
