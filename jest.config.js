module.exports = {
    preset: "ts-jest/presets/js-with-babel",
    moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
    transformIgnorePatterns: [
      "node_modules/(?!(react-dnd|ml-matrix|ml-array-rescale|@antv/*|dnd-core|react-dnd-html5-backend|three|@jupyterlab|@jupyter-widgets|@jupyterlab/.*)/)"
    ],
    globals: {
      "ts-jest": {
        tsConfig: "tsconfig.json"
      }
    },
    moduleNameMapper: {
      "^react-dnd-html5-backend$": "react-dnd-html5-backend/dist/cjs",
      "^react-dnd-touch-backend$": "react-dnd-touch-backend/dist/cjs",
      "^react-dnd-test-backend$": "react-dnd-test-backend/dist/cjs",
      "^react-dnd-test-utils$": "react-dnd-test-utils/dist/cjs",
      "^react-dnd$": "react-dnd/dist/ReactDnD.js",
      "^insert-css$" : "<rootDir>/src/utils/tests/func_mock.js",
      "^OrbitControls$": "three/examples/jsm/controls/OrbitControls.js",
      "\\.(css|less|sass|scss)$": "<rootDir>/src/utils/tests/style_mock.js",
      "\\.(gif|ttf|eot|svg)$": "<rootDir>/src/utils/tests/file_mock.js"
    },
    setupFiles: ["<rootDir>/src/utils/tests/test_env.ts", "jest-canvas-mock"],
    verbose : true
  };
  