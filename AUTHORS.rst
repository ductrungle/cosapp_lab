=======
Credits
=======

Development Lead
----------------

* `Etienne Lac <https://gitlab.com/etienne.lac>`_ (<etienne.lac@safrangroup.com>)

Contributors
------------

* `Duc Trung Le <https://gitlab.com/ductrungle>`_
* `Adrien Delsalle <https://gitlab.com/adriendelsalle>`_
* `Frederic Collonval <https://gitlab.com/fcollonval>`_
